package com.influans.team.mapper;

import com.influans.team.dto.CustomerDto;
import com.influans.team.entity.CustomerEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.modelmapper.TypeToken;
import java.util.List;

@Component
public class CustomerMapper {

    public CustomerDto convertToDto(CustomerEntity customerEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(customerEntity, CustomerDto.class);
    }

    public CustomerEntity convertToEntity(CustomerDto customerDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(customerDto, CustomerEntity.class);
    }

    public List<CustomerEntity> convertListToEntity(List<CustomerDto> customerDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(customerDto, new TypeToken<List<CustomerEntity>>() {}.getType());
    }

    public List<CustomerDto> convertListToDto(List<CustomerEntity> customerEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(customerEntity, new TypeToken<List<CustomerDto>>() {}.getType());
    }

}
