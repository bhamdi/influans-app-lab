package com.influans.team.repository;

import java.util.List;

import com.influans.team.entity.CustomerEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CustomerRepository extends ElasticsearchRepository<CustomerEntity, String> {

    public List<CustomerEntity> findByFirstName(String firstName);

    public List<CustomerEntity> findByLastName(String lastName);

}