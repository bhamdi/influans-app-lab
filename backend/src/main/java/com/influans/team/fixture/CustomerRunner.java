package com.influans.team.fixture;

import com.influans.team.dto.CustomerDto;
import com.influans.team.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements CommandLineRunner {

    private final CustomerService customerService;

    @Autowired
    CustomerRunner(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void run(String... args) throws Exception {
        this.customerService.deleteAll();
        saveCustomers();
    }

    private void saveCustomers() {
        this.customerService.saveCustomer(new CustomerDto("Alice", "Smith"));
        this.customerService.saveCustomer(new CustomerDto("Bob", "Smith"));
    }
}