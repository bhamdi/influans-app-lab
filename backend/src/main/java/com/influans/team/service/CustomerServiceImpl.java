package com.influans.team.service;

import com.influans.team.dto.CustomerDto;
import com.influans.team.util.ConverterUtil;
import com.influans.team.mapper.CustomerMapper;
import com.influans.team.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class CustomerServiceImpl implements CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    @Autowired
    CustomerServiceImpl(CustomerRepository customerRepository,
                        CustomerMapper customerMapper
    ) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public void deleteAll() {
        try {
            this.customerRepository.deleteAll();
            LOGGER.info("all customers are deleted");
        } catch (Exception e) {
            LOGGER.error("CustomerService - saveCustomer: "+ e.getMessage());
        }
    }

    @Override
    public void deleteCustomer(String id) {
        try {
            this.customerRepository.delete(id);
            LOGGER.info("delete customer with id {} deleted", id);
        } catch (Exception e) {
            LOGGER.error("CustomerService - saveCustomer: "+ e.getMessage());
        }
    }

    @Override
    public CustomerDto saveCustomer(CustomerDto customer) {
        try {
            return customerMapper.convertToDto(this.customerRepository.save(customerMapper.convertToEntity(customer)));
        } catch (Exception e) {
            LOGGER.error("CustomerService - saveCustomer: "+ e.getMessage());
            return null;
        }
    }

    @Override
    public boolean saveCustomers(List<CustomerDto> customers) {
        try {
            customers.forEach(customerDto -> saveCustomer(customerDto));
           // customers.forEach(this::saveCustomer);
            LOGGER.info("CustomerService - saveCustomers");
            return true;
        } catch (Exception e) {
            LOGGER.error("CustomerService - saveCustomers: "+ e.getMessage());
            return false;
        }
    }

    @Override
    public List<CustomerDto> fetchAll() {
        try {
            LOGGER.info("CustomerService - fetchAll");
            return customerMapper.convertListToDto(ConverterUtil.toList(this.customerRepository.findAll()));
        } catch (Exception e) {
            LOGGER.error("CustomerService - fetchAll: "+ e.getMessage());
            return null;
        }
    }

    @Override
    public CustomerDto fetchCustomer(String id) {
        try {
            LOGGER.info("CustomerService - fetchCustomer");
            return customerMapper.convertToDto(this.customerRepository.findOne(id));
        } catch (Exception e) {
            LOGGER.error("CustomerService - fetchCustomer: "+ e.getMessage());
            return null;
        }
    }
}
