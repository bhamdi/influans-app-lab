package com.influans.team.service.rest;

import com.influans.team.dto.CustomerDto;
import com.influans.team.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@Service
@RestController
@RequestMapping("/api/customers")
public class CustomerRestService {

    private final CustomerService customerService;

    @Autowired
    CustomerRestService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(method = RequestMethod.GET)
    List<CustomerDto> findCustomers() {
        return this.customerService.fetchAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    CustomerDto saveCustomer(@RequestBody CustomerDto customerDto) {
        return this.customerService.saveCustomer(customerDto);
    }

    @RequestMapping(value="{id}", method = RequestMethod.GET)
    @ResponseBody
    CustomerDto getCustomerByName(@PathParam("id") String id) {
        return this.customerService.fetchCustomer(id);
    }
}
