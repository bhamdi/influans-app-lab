package com.influans.team.service;

import com.influans.team.dto.CustomerDto;
import java.util.List;

public interface CustomerService {

    void deleteCustomer(String id);

    CustomerDto saveCustomer(CustomerDto customers);

    boolean saveCustomers(List<CustomerDto> customers);

    void deleteAll();

    List<CustomerDto> fetchAll();

    CustomerDto fetchCustomer(String id);
}
