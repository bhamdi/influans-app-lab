package com.influans.team;

import com.influans.team.fixture.CustomerRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamApplication {

	public static void main(String[] args) {

		SpringApplication.run(TeamApplication.class, args);
	}
}
